﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ПР2_Гнутикова
{
    public class Nomer1
    {
       public static void Nomer()
        {
            Console.WriteLine("Номер 1.1 (цикл for)");
            for (int i = 0; i < 5; i++) 
            {
                Console.WriteLine(i);
            }
            
            Console.WriteLine("Номер 1.2 (цикл for)");
            int[] numbers = { 4, 7, 1, 23, 43 };
            int s = 0;
            for (int a = 0; a < numbers.Length; a++)
            {
                s += numbers[a];
            }
            Console.WriteLine(s);

            Console.WriteLine("Номер 1.3 (цикл for)");
            for (int z = 5; z > 0; z--) //выполнится 5 раз
            {
                Console.WriteLine(z);
            }
            
            Console.WriteLine("Номер 1.4 (цикл for)");
            for (int c = 0; c <= 50; c += 2) 
            {
                Console.WriteLine(c);
            }
           
            Console.WriteLine("Номер 1.5 (цикл while)");
            int v = 0;
            while (v < 5) //while (true) для бесконечного цикла
            {
                Console.WriteLine(v);
                v++;
            }
            
            Console.WriteLine("Номер 1.6 (цикл do-while)");
            int number;
            do
            {
                Console.WriteLine("Введите число 5");
                number = Convert.ToInt32(Console.ReadLine());
            }
            while (number != 5);
            Console.ReadKey();
            Console.WriteLine("Номер 1.7 (цикл break )");
            int[] e = { 4, 7, 13, 20, 33, 23, 54 };
            bool b = false;
            for (int i = 0; i < e.Length; i++)
            {
                if (e[i] % 13 == 0)
                {
                    b = true;
                    break;
                }
            }
            Console.WriteLine(b ? "В массиве есть число кратное 13" : "В массиве нет числа кратного 13");
            
            Console.WriteLine("Номер 1.8 (цикл continue)");
            int[] m = { 4, 7, 13, 20, 33, 23, 54 };
            int n = 0;
            for (int i = 0; i < m.Length; i++)
            {
                if (m[i] % 2 == 0)
                    continue; 
                n += m[i];
            }
            Console.WriteLine(n);
            Console.WriteLine(" ");
        }

    }
}
