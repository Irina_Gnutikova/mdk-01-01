﻿
namespace ПР6_Гнутикова
{
    partial class Click
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClick = new System.Windows.Forms.Button();
            this.tmr = new System.Windows.Forms.Timer(this.components);
            this.lbltime = new System.Windows.Forms.Label();
            this.lblclick = new System.Windows.Forms.Label();
            this.tmrclick = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btnClick
            // 
            this.btnClick.Font = new System.Drawing.Font("Viner Hand ITC", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.btnClick.Location = new System.Drawing.Point(365, 328);
            this.btnClick.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClick.Name = "btnClick";
            this.btnClick.Size = new System.Drawing.Size(193, 95);
            this.btnClick.TabIndex = 0;
            this.btnClick.Text = "Кликни!";
            this.btnClick.UseVisualStyleBackColor = true;
            this.btnClick.Click += new System.EventHandler(this.btnClick_Click);
            // 
            // tmr
            // 
            this.tmr.Interval = 1000;
            this.tmr.Tick += new System.EventHandler(this.tmr_Tick);
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.Font = new System.Drawing.Font("Viner Hand ITC", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbltime.Location = new System.Drawing.Point(505, 9);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(314, 37);
            this.lbltime.TabIndex = 2;
            this.lbltime.Text = "Оставшееся время: 60 сек";
            this.lbltime.Click += new System.EventHandler(this.lbltime_Click);
            // 
            // lblclick
            // 
            this.lblclick.AutoSize = true;
            this.lblclick.Font = new System.Drawing.Font("Viner Hand ITC", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblclick.Location = new System.Drawing.Point(44, 9);
            this.lblclick.Name = "lblclick";
            this.lblclick.Size = new System.Drawing.Size(259, 37);
            this.lblclick.TabIndex = 3;
            this.lblclick.Text = "Вы кликнули: 0 раз(а)";
            this.lblclick.Click += new System.EventHandler(this.lblclick_Click);
            // 
            // tmrclick
            // 
            this.tmrclick.Enabled = true;
            this.tmrclick.Tick += new System.EventHandler(this.tmrclick_Tick);
            // 
            // Click
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 600);
            this.Controls.Add(this.lblclick);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.btnClick);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Click";
            this.Text = "Click me";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClick;
        private System.Windows.Forms.Timer tmr;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label lblclick;
        private System.Windows.Forms.Timer tmrclick;
    }
}

