﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ПР6_Гнутикова
{
    public partial class Click : Form
    {
        public Click()
        {
            InitializeComponent();
        }
        int m = 0;
        private void btnClick_Click(object sender, EventArgs e)
        {
            Random rnd = new();
            int rndLocX = rnd.Next(50, 550);
            int rndLocY = rnd.Next(100, 350);
            btnClick.Top = rndLocY;
            btnClick.Left = rndLocX;
            m++;
            if (tmr.Enabled != true)
            {
                tmr.Enabled = true; 
            }
        }
        int c = 60;

        private void tmr_Tick(object sender, EventArgs e)
        {
            
            if (c==0)
            {
                tmr.Enabled = false;
                btnClick.Top = 328;
                btnClick.Left = 365;
                lbltime.Text = "Оставшееся время: 60 сек";
                MessageBox.Show($"Вы кликнули {m} раз(а)", "Время вышло!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                m = 0;
                c = 60;
            }
            else
            {
                c--;
                lbltime.Text = $"Оставшееся время: {c} сек";
            }
        }
        private void lbltime_Click(object sender, EventArgs e)
        {

        }
        private void lblclick_Click(object sender, EventArgs e)
        {

        }
        private void tmrclick_Tick(object sender, EventArgs e)
        {
            if (tmr.Enabled == true)
            {
                lblclick.Text = $"Вы кликнули: {m} раз(а)";
            }  
            else
            {
                lblclick.Text = "Вы кликнули: 0 раз";
            }
        }
    }
}
