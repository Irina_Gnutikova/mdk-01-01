﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ПР7_Гнутикова
{
    public partial class fUser : Form
    {
        public fUser()
        {
            InitializeComponent();
        }
        string[,] user = new string[,] {{ "Фамилия Имя: Сафронов Дмитрий", "Дата рождения: 26.12.1996" }, { "Фамилия Имя: Николаев Александр", "Дата рождения: 15.03.1997" },{ "Фамилия Имя: Пашаян Евгения", "Дата рождения: 25.09.2001" } };
        bool c = true;
        
        private void btnEnter_Click(object sender, EventArgs e)
        {
            
            if(tbLogin.Text.Trim() != "" & tbPassword.Text.Trim() != "")
            {
                if (c)
                {
                    if ((tbLogin.Text == "da_safronov" | tbLogin.Text == "aa_nikolaev" | tbLogin.Text == "er_pashayan") | (tbPassword.Text == "theAdmin" | tbPassword.Text == "tHuGlife" | tbPassword.Text == "2022Hello2022"))
                    {
                       if (tbLogin.Text == "da_safronov" & tbPassword.Text == "theAdmin")
                        {
                            tbRole.Text = "Администратор";
                            tbRole.BackColor = Color.YellowGreen;
                            lbxInfo.Items.Add(user[0, 0]);
                            lbxInfo.Items.Add(user[0, 1]);
                        }   
                        if (tbLogin.Text == "aa_nikolaev" & tbPassword.Text == "tHuGlife")
                        {
                            tbRole.Text = "Разработчик";
                            tbRole.BackColor = Color.YellowGreen;
                            lbxInfo.Items.Add(user[1,0]);
                            lbxInfo.Items.Add(user[1, 1]);
                        }
                        if (tbLogin.Text == "er_pashayan" & tbPassword.Text == "2022Hello2022")
                        {
                            tbRole.Text = "Пользователь";
                            tbRole.BackColor = Color.YellowGreen;
                            lbxInfo.Items.Add(user[2,0]);
                            lbxInfo.Items.Add(user[2, 1]);
                        }
                        btnEnter.Text = "Выйти";
                        c = !c; 
                    }
                    else
                    {  
                        MessageBox.Show("Ошибка в логине или пароле", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } 
                }
                else
                {
                    tbRole.BackColor = Color.White;
                    tbRole.Clear();
                    btnEnter.Text = "Войти";
                    c = true;
                    tbLogin.Clear();
                    tbPassword.Clear();
                    lbxInfo.Items.Clear();
                }
            }
            else
            {
                MessageBox.Show("Введены не все данные!","Error",MessageBoxButtons.OK,MessageBoxIcon.Error); 
            }
        }

        private void tbLogin_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbxInfo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tbRole_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
