﻿
namespace ПР7_Гнутикова
{
    partial class fUser
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxInfo = new System.Windows.Forms.ListBox();
            this.btnEnter = new System.Windows.Forms.Button();
            this.lblogin = new System.Windows.Forms.Label();
            this.lbpassword = new System.Windows.Forms.Label();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.lbRole = new System.Windows.Forms.Label();
            this.tbRole = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbxInfo
            // 
            this.lbxInfo.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbxInfo.FormattingEnabled = true;
            this.lbxInfo.ItemHeight = 24;
            this.lbxInfo.Location = new System.Drawing.Point(369, 85);
            this.lbxInfo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbxInfo.Name = "lbxInfo";
            this.lbxInfo.Size = new System.Drawing.Size(342, 244);
            this.lbxInfo.TabIndex = 0;
            this.lbxInfo.SelectedIndexChanged += new System.EventHandler(this.lbxInfo_SelectedIndexChanged);
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("Candara", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnEnter.Location = new System.Drawing.Point(56, 227);
            this.btnEnter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(235, 69);
            this.btnEnter.TabIndex = 1;
            this.btnEnter.Text = "Войти";
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // lblogin
            // 
            this.lblogin.AutoSize = true;
            this.lblogin.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblogin.Location = new System.Drawing.Point(35, 85);
            this.lblogin.Name = "lblogin";
            this.lblogin.Size = new System.Drawing.Size(68, 24);
            this.lblogin.TabIndex = 2;
            this.lblogin.Text = "Логин:";
            // 
            // lbpassword
            // 
            this.lbpassword.AutoSize = true;
            this.lbpassword.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbpassword.Location = new System.Drawing.Point(26, 159);
            this.lbpassword.Name = "lbpassword";
            this.lbpassword.Size = new System.Drawing.Size(80, 24);
            this.lbpassword.TabIndex = 3;
            this.lbpassword.Text = "Пароль:";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(121, 159);
            this.tbPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(170, 27);
            this.tbPassword.TabIndex = 5;
            this.tbPassword.TextChanged += new System.EventHandler(this.tbPassword_TextChanged);
            // 
            // tbLogin
            // 
            this.tbLogin.Location = new System.Drawing.Point(121, 85);
            this.tbLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(170, 27);
            this.tbLogin.TabIndex = 6;
            this.tbLogin.TextChanged += new System.EventHandler(this.tbLogin_TextChanged);
            // 
            // lbRole
            // 
            this.lbRole.AutoSize = true;
            this.lbRole.Location = new System.Drawing.Point(352, 36);
            this.lbRole.Name = "lbRole";
            this.lbRole.Size = new System.Drawing.Size(108, 20);
            this.lbRole.TabIndex = 7;
            this.lbRole.Text = "Текущая роль:";
            // 
            // tbRole
            // 
            this.tbRole.Location = new System.Drawing.Point(466, 33);
            this.tbRole.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbRole.Name = "tbRole";
            this.tbRole.Size = new System.Drawing.Size(209, 27);
            this.tbRole.TabIndex = 8;
            this.tbRole.TextChanged += new System.EventHandler(this.tbRole_TextChanged);
            // 
            // fUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 379);
            this.Controls.Add(this.tbRole);
            this.Controls.Add(this.lbRole);
            this.Controls.Add(this.tbLogin);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.lbpassword);
            this.Controls.Add(this.lblogin);
            this.Controls.Add(this.btnEnter);
            this.Controls.Add(this.lbxInfo);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "fUser";
            this.Text = "Users";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbxInfo;
        private System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.Label lblogin;
        private System.Windows.Forms.Label lbpassword;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.Label lbRole;
        private System.Windows.Forms.TextBox tbRole;
    }
}

