﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ПР4_Гнутикова
{
    public class Other : IComputer_Class
    {
        int others = 13;
        string OtherInfo = "Компьютерные мыши, клавиатуры и прочее, расчитанное на 12 мест для учеников и одно для учителя.";

        int IComputer_Class.Kolvo()
        {
            return others;
        }
        string IComputer_Class.Info()
        {
            return OtherInfo;
        }
        public static void Information()
        {
            IComputer_Class o = new Other();

            Console.WriteLine($"Другое; Количество по: {o.Kolvo()}; Описание: {o.Info()}");
        }
    }
}
