﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ПР4_Гнутикова
{
    public class ElBoard : IComputer_Class
    {
        int boards = 1;
        string BoardInfo = "Электронная доска для показа учебного материала ученикам.";

        int IComputer_Class.Kolvo()
        {
            return boards;
        }
        string IComputer_Class.Info()
        {
            return BoardInfo;
        }
        public static void Information()
        {
            IComputer_Class eb = new ElBoard();

            Console.WriteLine($"Электронная доска; Количество: {eb.Kolvo()}; Описание: {eb.Info()}");
        }
    }
}
