﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ПР4_Гнутикова
{
    public class Chair : IComputer_Class
    {
        int chairs = 13;
        string ChairInfo = "Стул на металической ножке и на колесиках. Имеются ручки по бокам. Спинка, сидение и частично ручки стула обиты серебристым материалом, обеспечивающая мягкость сидящему человеку.";
        int IComputer_Class.Kolvo()
        {
            return chairs;
        }
        string IComputer_Class.Info()
        {
            return ChairInfo;
        }
        public static void Information()
        {
            IComputer_Class c = new Chair();

            Console.WriteLine($"Стулья; Количество: {c.Kolvo()}; Описание: {c.Info()}");
        }
    }
}
