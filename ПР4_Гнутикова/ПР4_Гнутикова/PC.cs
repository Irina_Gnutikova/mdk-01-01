﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ПР4_Гнутикова
{
    public class PC : IComputer_Class
    {
        int pcs = 13;
        string PCInfo = "Компьютер средней мощности с возможностью создавания на нем различных приложений, игр, сайтов и т.д..";

        int IComputer_Class.Kolvo()
        {
            return pcs;
        }
        string IComputer_Class.Info()
        {
            return PCInfo;
        }
        public static void Information()
        {
            IComputer_Class pc = new PC();

            Console.WriteLine($"Компьютеры; Количество: {pc.Kolvo()}; Описание: {pc.Info()}");
        }
    }
}
