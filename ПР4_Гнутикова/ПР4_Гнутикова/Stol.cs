﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ПР4_Гнутикова
{
     public class Stol : IComputer_Class
    {
        int stols=13;
        string StolInfo = "Деревянный стол с отделением под вещи синего цвета с темными элементами. Сверху находится стекло, полностью покрывающее его в целях защиты от царапин и прочего. В комплекте входит подставка на колесиках для системного блока компьютера.";
        
        int IComputer_Class.Kolvo()
        {
            return stols;
        }
        string IComputer_Class.Info()
        {
            return StolInfo;
        }
        public static void Information()
        {
            IComputer_Class s = new Stol();
            
            Console.WriteLine($"Столы; Количество: {s.Kolvo()}; Описание: {s.Info()}");
        }
    }
}
