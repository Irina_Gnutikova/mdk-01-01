﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ПР3_Гнутикова,Гусева
{
    class Program
    {
        static void Main(string[] args)
        {
            Pupil[] A = new Pupil[3];
            int i = 0;
            while (i < 3)
            {
                Pupil pupil = new Pupil();
                Console.WriteLine("Введите фамилию");
                pupil.SecondName = Console.ReadLine();
                Console.WriteLine("Введите имя");
                pupil.FirstName = Console.ReadLine();
                Console.WriteLine("Введите группу");
                pupil.GroupName = Console.ReadLine();
                Console.WriteLine("Введите возраст");
                pupil.Year = Console.ReadLine();
                A[i] = pupil;
                i++;
            }

            string q = "";
            foreach (Pupil c in A)
            {
                if (q != c.GroupName)
                {
                    Console.WriteLine($"Имя группы:{c.GroupName}");
                    
                }
                Console.WriteLine($"{c.FirstName} {c.SecondName} - {c.Year} лет");
                q = c.GroupName;
            }
        }
    }
}