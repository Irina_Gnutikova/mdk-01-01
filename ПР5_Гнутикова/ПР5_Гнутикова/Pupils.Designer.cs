﻿
namespace ПР5_Гнутикова
{
    partial class Pupils
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdd = new System.Windows.Forms.Button();
            this.listPupil = new System.Windows.Forms.ListBox();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.tbSecondName = new System.Windows.Forms.TextBox();
            this.tbGroupName = new System.Windows.Forms.TextBox();
            this.tbYear = new System.Windows.Forms.TextBox();
            this.lbFirstName = new System.Windows.Forms.Label();
            this.lbSecondName = new System.Windows.Forms.Label();
            this.lbYear = new System.Windows.Forms.Label();
            this.lbGroupName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnAdd.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnAdd.Location = new System.Drawing.Point(406, 164);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(331, 90);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Добавить ученика в список\r\n";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // listPupil
            // 
            this.listPupil.BackColor = System.Drawing.SystemColors.HighlightText;
            this.listPupil.FormattingEnabled = true;
            this.listPupil.ItemHeight = 20;
            this.listPupil.Location = new System.Drawing.Point(39, 30);
            this.listPupil.Name = "listPupil";
            this.listPupil.Size = new System.Drawing.Size(317, 224);
            this.listPupil.TabIndex = 1;
            this.listPupil.SelectedIndexChanged += new System.EventHandler(this.listPupil_SelectedIndexChanged);
            // 
            // tbFirstName
            // 
            this.tbFirstName.Location = new System.Drawing.Point(645, 64);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(125, 27);
            this.tbFirstName.TabIndex = 2;
            this.tbFirstName.TextChanged += new System.EventHandler(this.tbFirstName_TextChanged);
            // 
            // tbSecondName
            // 
            this.tbSecondName.Location = new System.Drawing.Point(444, 64);
            this.tbSecondName.Name = "tbSecondName";
            this.tbSecondName.Size = new System.Drawing.Size(147, 27);
            this.tbSecondName.TabIndex = 3;
            this.tbSecondName.TextChanged += new System.EventHandler(this.tbSecondName_TextChanged);
            // 
            // tbGroupName
            // 
            this.tbGroupName.Location = new System.Drawing.Point(529, 113);
            this.tbGroupName.Name = "tbGroupName";
            this.tbGroupName.Size = new System.Drawing.Size(62, 27);
            this.tbGroupName.TabIndex = 4;
            this.tbGroupName.TextChanged += new System.EventHandler(this.tbGroupName_TextChanged);
            // 
            // tbYear
            // 
            this.tbYear.Location = new System.Drawing.Point(683, 113);
            this.tbYear.Name = "tbYear";
            this.tbYear.Size = new System.Drawing.Size(54, 27);
            this.tbYear.TabIndex = 5;
            this.tbYear.TextChanged += new System.EventHandler(this.tbYear_TextChanged);
            // 
            // lbFirstName
            // 
            this.lbFirstName.AutoSize = true;
            this.lbFirstName.Location = new System.Drawing.Point(597, 67);
            this.lbFirstName.Name = "lbFirstName";
            this.lbFirstName.Size = new System.Drawing.Size(42, 20);
            this.lbFirstName.TabIndex = 6;
            this.lbFirstName.Text = "Имя:";
            // 
            // lbSecondName
            // 
            this.lbSecondName.AutoSize = true;
            this.lbSecondName.Location = new System.Drawing.Point(362, 67);
            this.lbSecondName.Name = "lbSecondName";
            this.lbSecondName.Size = new System.Drawing.Size(76, 20);
            this.lbSecondName.TabIndex = 7;
            this.lbSecondName.Text = "Фамилия:";
            // 
            // lbYear
            // 
            this.lbYear.AutoSize = true;
            this.lbYear.Location = new System.Drawing.Point(597, 116);
            this.lbYear.Name = "lbYear";
            this.lbYear.Size = new System.Drawing.Size(67, 20);
            this.lbYear.TabIndex = 9;
            this.lbYear.Text = "Возраст:";
            // 
            // lbGroupName
            // 
            this.lbGroupName.AutoSize = true;
            this.lbGroupName.Location = new System.Drawing.Point(453, 116);
            this.lbGroupName.Name = "lbGroupName";
            this.lbGroupName.Size = new System.Drawing.Size(61, 20);
            this.lbGroupName.TabIndex = 10;
            this.lbGroupName.Text = "Группа:";
            // 
            // Pupils
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 293);
            this.Controls.Add(this.lbGroupName);
            this.Controls.Add(this.lbYear);
            this.Controls.Add(this.lbSecondName);
            this.Controls.Add(this.lbFirstName);
            this.Controls.Add(this.tbYear);
            this.Controls.Add(this.tbGroupName);
            this.Controls.Add(this.tbSecondName);
            this.Controls.Add(this.tbFirstName);
            this.Controls.Add(this.listPupil);
            this.Controls.Add(this.btnAdd);
            this.Name = "Pupils";
            this.Text = "Ученики";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox listPupil;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.TextBox tbSecondName;
        private System.Windows.Forms.TextBox tbGroupName;
        private System.Windows.Forms.TextBox tbYear;
        private System.Windows.Forms.Label lbFirstName;
        private System.Windows.Forms.Label lbSecondName;
        private System.Windows.Forms.Label lbYear;
        private System.Windows.Forms.Label lbGroupName;
    }
}

