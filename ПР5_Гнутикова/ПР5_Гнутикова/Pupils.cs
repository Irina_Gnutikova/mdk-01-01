﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ПР5_Гнутикова
{
    public partial class Pupils : Form
    {
        public Pupils()
        {
            InitializeComponent();
        }
        string lustGroup = "";
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Pupils pupil = new Pupils();
            pupil.FirstName = tbFirstName.Text;
            pupil.SecondName = tbSecondName.Text;
            pupil.Year = tbYear.Text;
            pupil.GroupName = tbGroupName.Text;
            

            if (tbFirstName.Text.Trim(' ') != "" & tbSecondName.Text.Trim(' ') != "" & tbGroupName.Text.Trim(' ') != "" & tbYear.Text.Trim(' ') != "")
            {
                
                if (lustGroup != tbGroupName.Text)
                { 
                    listPupil.Items.Add($"Имя группы: {pupil.GroupName};");
                    lustGroup = tbGroupName.Text;
                }  
                listPupil.Items.Add($" {pupil.FirstName} {pupil.SecondName} - {pupil.Year} лет");
            }
            else
            {
                MessageBox.Show("Введены не все данные!", "ОШИБКА!", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
            tbGroupName.Clear();
            tbFirstName.Clear();
            tbSecondName.Clear();
            tbYear.Clear();
        }

        private void tbFirstName_TextChanged(object sender, EventArgs e)
        {
        }

        private void tbSecondName_TextChanged(object sender, EventArgs e)
        {
        }

        private void tbGroupName_TextChanged(object sender, EventArgs e)
        {
        }

        private void tbYear_TextChanged(object sender, EventArgs e)
        {
        }

        private void listPupil_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
